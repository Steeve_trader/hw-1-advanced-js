import gulp from "gulp";
import htmlmin from "gulp-htmlmin";
import terser from "gulp-terser";
import clean from "gulp-clean";
import browserSync from "browser-sync";
import concat from "gulp-concat";

const cleanDist = () => {
    return gulp.src('./dist', { read: false })
        .pipe(clean());
};


const html = () => {
    return gulp.src('./src/**/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest("./"));
};

const js = () => {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(terser())
        .pipe(gulp.dest("./dist/js"));
};

const dev = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('./src/**/*', gulp.series(cleanDist, gulp.parallel(html, js), (next) => {
        browserSync.reload(),
            next();
    }));
};

gulp.task('html', html);
gulp.task('js', js);
gulp.task('cleanDist', cleanDist);

gulp.task('build', gulp.parallel(html, js));
gulp.task('dev', gulp.series(cleanDist, 'build', dev));